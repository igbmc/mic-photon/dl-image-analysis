---
title: "Deep Learning in Image Analysis"
subtitle: "_How DL has revolutionized the field <br> with a couple of examples_"
author: "Marco Dalla Vecchia"
institute: "IGBMC"
date: "05-15-2023"
date-format: long
format: 
    revealjs:
        theme: default
        self-contained: false
        slide-number: true
        chalkboard: true
        preview-links: auto
        logo: "assets/images/igbmc.png"
        preload-iframes: true
        center: true
revealjs-plugins:
  - pointer
---

## Disclaimers
- I am "_just_" a biologist
- I apply methods not create from scratch
- I learned along the way, please correct me if I'm wrong {{< fa face-smile-wink >}}

# Deep Learning fundamentals
## On the previous episode
![[Source](https://serokell.io/files/zx/zxwju3ha.Machine-learning-vs-deep-learning.jpg)](https://serokell.io/files/zx/zxwju3ha.Machine-learning-vs-deep-learning.jpg)

## Neural Networks
Input layer &rarr; Hidden layer &rarr; Output layer

![[Source](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Neural_network.svg/600px-Neural_network.svg.png)](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Neural_network.svg/600px-Neural_network.svg.png)

## Neural Networks

```{=html}
<iframe src="https://playground.tensorflow.org/#activation=tanh&batchSize=10&dataset=circle&regDataset=reg-plane&learningRate=0.03&regularizationRate=0&noise=0&networkShape=4,2&seed=0.84592&showTestData=false&discretize=false&percTrainData=50&x=true&y=true&xTimesY=false&xSquared=false&ySquared=false&cosX=false&sinX=false&cosY=false&sinY=false&collectStats=false&problem=classification&initZero=false&hideText=false" title="tensorflow-playground" width="100%" height="600px"></iframe>
```

## Neural Networks

```{=html}
<iframe src="https://playground.tensorflow.org/#activation=tanh&batchSize=10&dataset=spiral&regDataset=reg-plane&learningRate=0.03&regularizationRate=0&noise=0&networkShape=8,4,2&seed=0.14057&showTestData=false&discretize=false&percTrainData=50&x=true&y=true&xTimesY=true&xSquared=true&ySquared=true&cosX=false&sinX=true&cosY=false&sinY=true&collectStats=false&problem=classification&initZero=false&hideText=false" title="tensorflow-playground" width="100%" height="600px"></iframe>
```

## Deep learning

Many, many, many hidden layers..

![[Source](https://miro.medium.com/v2/resize:fit:1199/1*N8UXaiUKWurFLdmEhEHiWg.jpeg)](https://miro.medium.com/v2/resize:fit:1199/1*N8UXaiUKWurFLdmEhEHiWg.jpeg)

## Convolutional Neural Network (CNN)
![[Source](https://editor.analyticsvidhya.com/uploads/90650dnn2.jpeg)](https://editor.analyticsvidhya.com/uploads/90650dnn2.jpeg)

## Convolution
Convolutional layer reduces the high dimensionality of images without losing its information.

![[Source](https://miro.medium.com/v2/resize:fit:1400/1*Fw-ehcNBR9byHtho-Rxbtw.gif)](https://miro.medium.com/v2/resize:fit:1400/1*Fw-ehcNBR9byHtho-Rxbtw.gif)

## U-Net {.smaller}
CNN developed for biomedical image segmentation at the Computer Science Department of the University of Freiburg.

![[Source](https://lmb.informatik.uni-freiburg.de/people/ronneber/u-net/)](https://lmb.informatik.uni-freiburg.de/people/ronneber/u-net/u-net-architecture.png)

## 2 classes: background vs foreground
```{=html}
<iframe src="https://playground.tensorflow.org/#activation=tanh&batchSize=10&dataset=xor&regDataset=reg-plane&learningRate=0.03&regularizationRate=0&noise=0&networkShape=4,2&seed=0.51444&showTestData=false&discretize=true&percTrainData=50&x=true&y=true&xTimesY=true&xSquared=true&ySquared=true&cosX=false&sinX=false&cosY=false&sinY=false&collectStats=false&problem=classification&initZero=false&hideText=false" title="tensorflow-playground" width="100%" height="600px"></iframe>
```

## How do we extract individual objects from an image?

:::: {.columns}
::: {.column}
![](assets/images/segmentation_bottom_up.png)
:::
::: {.column}
![](assets/images/segmentation_top_down.png)
:::
::::

## How do we tell apart crowded objects?

:::: {.columns}
::: {.column}
![](assets/images/segmentation_bottom_up_problems.png)
:::
::: {.column}
![](assets/images/segmentation_top_down_problems.png)
:::
::::

## _Traditional_ vs DL methods
:::: {.columns}
::: {.column width="50%"}
### _"Traditional"_
- _A priori_ knowledge (mechanisms, dynamics, models ..)
- Combinations of filters and algorithms
- Heavily dependent on parameter tweaking
- Self or expert-supervised
:::

::: {.column width="50%"}
### Deep Learning
- Relies on large amount of data
- Little _a priori_ knowledge needed
- Heavy quality control before/after
- Mostly self-supervised
:::
::::

# How can DL help in Image Analysis?
## Image Analysis common tasks {auto-animate=true}

{{< fa object-group >}} Object Detection

{{< fa images >}} Semantic Segmentation

{{< fa people-arrows >}} Instance Segmentation

![[Source](https://blog.kakaocdn.net/dn/CRvWU/btqSsQypZlk/gAMakLhRAykcULSIsSaP60/img.png)](https://blog.kakaocdn.net/dn/CRvWU/btqSsQypZlk/gAMakLhRAykcULSIsSaP60/img.png)

## {auto-animate=true}

[{{< fa object-group >}} Object Detection]{style="color:#002562;text-decoration:underline"}

Detection and shape categorization of lipid droplets in _C. elegans_.

![_Data source: Julien Lambert (Jarriault's lab) IGBMC_](assets/images/yolo_celegans.jpg)

## Image Analysis common tasks {auto-animate=true}

{{< fa object-group >}} Object Detection

{{< fa images >}} Semantic Segmentation

{{< fa people-arrows >}} Instance Segmentation

![[Source](https://blog.kakaocdn.net/dn/CRvWU/btqSsQypZlk/gAMakLhRAykcULSIsSaP60/img.png)](https://blog.kakaocdn.net/dn/CRvWU/btqSsQypZlk/gAMakLhRAykcULSIsSaP60/img.png)

## {auto-animate=true}

[{{< fa images >}} Semantic Segmentation]{style="color:#002562;text-decoration:underline"}

Detection of injured tissue in muscle sections.

![_Data source: Joe Rizk (Metzger's lab) IGBMC_](assets/images/muscle_section.png)

## {auto-animate=true}

[{{< fa images >}} Semantic Segmentation]{style="color:#002562;text-decoration:underline"}

Detection of injured tissue in muscle sections.

![_Data source: Joe Rizk (Metzger's lab) IGBMC_](assets/images/muscle_section_classified.png)

## Image Analysis common tasks {auto-animate=true}

{{< fa object-group >}} Object Detection

{{< fa images >}} Semantic Segmentation

{{< fa people-arrows >}} Instance Segmentation

![[Source](https://blog.kakaocdn.net/dn/CRvWU/btqSsQypZlk/gAMakLhRAykcULSIsSaP60/img.png)](https://blog.kakaocdn.net/dn/CRvWU/btqSsQypZlk/gAMakLhRAykcULSIsSaP60/img.png)

# Instance Segmentation
## _Traditional_ method: thresholding

Raw Image &rarr; Gaussian Blur &rarr; Otsu thresholding &rarr; Fill holes

![[Data Source](https://github.com/ahklemm/ImageJMacro_Introduction)](assets/images/segmentation/threshold_segmentation.jpg)


## _Traditional_ method: watershed
![[Source](https://imagej.net/media/plugins/watershed-flooding-graph.png)](https://imagej.net/media/plugins/watershed-flooding-graph.png){fig-align="center"}

## _Traditional_ method: watershed
![[Source](https://scikit-image.org/docs/stable/_images/sphx_glr_plot_watershed_001.png)](https://scikit-image.org/docs/stable/_images/sphx_glr_plot_watershed_001.png){fig-align="center"}

## _Traditional_ method: watershed
![[Source](https://docs.opencv.org/4.x/water_result.jpg)](https://docs.opencv.org/4.x/water_result.jpg)

## _Traditional_ method: watershed 3D
![[Data Source](https://scikit-image.org/docs/stable/api/skimage.data.html#skimage.data.cells3d)](assets/images/segmentation/nuclei_3d_watershed.png){fig-align="center"}

## _Traditional_ method: watershed 3D
![[Data Source](https://scikit-image.org/docs/stable/api/skimage.data.html#skimage.data.cells3d)](assets/images/segmentation/nuclei_3d_watershed_labels.png){fig-align="center"}


## DL method: Stardist
> Object Detection with Star-convex Shapes

![[Source](https://github.com/stardist/stardist)](https://raw.githubusercontent.com/stardist/stardist/master/images/overview_2d.png)

## DL method: Stardist

![[Source](https://github.com/stardist/stardist)](https://raw.githubusercontent.com/stardist/stardist/master/images/example_steps.png)

## Stardist

Only works for **star-convex** shapes!

![[Source](https://www.researchgate.net/publication/331391642/figure/fig1/AS:941816836784128@1601558040162/a-Convex-set-b-star-shaped-and-c-not-star-shaped-set.png)](https://www.researchgate.net/publication/331391642/figure/fig1/AS:941816836784128@1601558040162/a-Convex-set-b-star-shaped-and-c-not-star-shaped-set.png)

## Stardist

Only works for **star-convex** shapes!

![](assets/images/segmentation/ground_truth.png){fig-align="center"}

## Stardist

Only works for **star-convex** shapes!

![](assets/images/segmentation/stardist_polygons.png){fig-align="center"}

## Stardist: 3D images

![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/nuclei_3d.jpg)

## Stardist: 3D images

![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/nuclei_3d_stardist.jpg)

## EmbedSeg
![[Source](https://juglab.github.io/EmbedSeg/)](https://juglab.github.io/EmbedSeg/resources/images/teaser.gif)

## EmbedSeg: 3D images

![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/nuclei_3d_embedseg.jpg)

## Cellpose
> a generalist algorithm for cellular segmentation

![[Source](https://t.co/kBMXmPp3Yn?amp=1)](assets/images/cellpose-versatility/cellpose_figure1a.jpg)

## Cellpose

![[Source](https://t.co/kBMXmPp3Yn?amp=1)](assets/images/cellpose-versatility/cellpose_figure1b.jpg)

## Cellpose versatility

:::: {.columns}
::: {.column}
![[Data Source](http://www.cellpose.org/)](assets/images/cellpose-versatility/img00.png)
:::
::: {.column}
![[cellpose.com](http://www.cellpose.org/)](assets/images/cellpose-versatility/img00_outlines.png)
:::
::::

## Cellpose versatility

:::: {.columns}
::: {.column}
![Data Source: Camilla Guiducci](assets/images/cellpose-versatility/hct116_18.png)
:::
::: {.column}
![(Riveline's lab IGBMC)](assets/images/cellpose-versatility/hct116_18_overlay.jpg)
:::
::::

## Cellpose versatility

:::: {.columns}
::: {.column}
![Data Source: Sebastien Moliere](assets/images/cellpose-versatility/muscle_fibers.jpg){width="90%"}
:::
::: {.column}
![(Tomasetto's lab IGBMC)](assets/images/cellpose-versatility/muscle_fibers_labels.jpg){width="90%"}
:::
::::

## Cellpose versatility

:::: {.columns}
::: {.column}
![](assets/images/cellpose-versatility/floor.png)
:::
::: {.column}
![](assets/images/cellpose-versatility/floor_outlines.png)
:::
::::

## Stardist Vs Cellpose
![](assets/images/segmentation/stardist_vs_cellpose_2d.png)

## Combining Stardist and Cellpose
![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/rnafish_3d.jpg)

## Combining Stardist and Cellpose {.smaller}
Segment every Z slice image with cellpose and curate it.

![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/rnafish_3d_cellpose_curated.jpg)

## Combining Stardist and Cellpose {.smaller}
Use the cellpose segmentation to train StarDist 3D.

![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/rnafish_3d_stardist.jpg)

## Combining Stardist and Cellpose {.smaller}
Find RNA-FISH puncta.

![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/rnafish_puncta_detection.gif)

## Combining Stardist and Cellpose {.smaller}
Assign them to each segmented nucleus.

![_Data source: Faezeh Forouzan Far (Mendoza's lab) IGBMC_](assets/images/segmentation/rnafish_3d_pernucleus.jpg)

# Zebrafish detection and behavioural analysis {.smaller}

_Data source: Mathieu Massonot (Golzio's lab) IGBMC_


## Example video
![](assets/movies/video_to_track.mp4){width="100%" autoplay="True"}

## _Traditional_ method: bg removal
![Median Image to identify the "**background**"](assets/images/fish-tracking/01-bg-image.jpg)

## _Traditional_ method: pre-processing
![Background subtraction and other pre-processing (gaussian blur, other filters etc..)](assets/images/fish-tracking/02-after-bg-sub.jpg)

## _Traditional_ method: object detection
![Threshold of processed image identifies object from binary image](assets/images/fish-tracking/03-thresholded-image.jpg)

## _Traditional_ method: problem??
![](assets/images/fish-tracking/04-fish-reflections.jpg)

## _Traditional_ method: problem??
![](assets/images/fish-tracking/04-objects-reflections.jpg)

## _Traditional_ method: tracking
![](assets/movies/example_tracking.mp4){width="100%" autoplay="True"}


## DeepLabCut
:::: {.columns}
::: {.column}
![](https://images.squarespace-cdn.com/content/v1/57f6d51c9f74566f55ecf271/1548190681885-AFQ70VARHO87KO4N1DO9/MouseLocomotion_warren.gif){fig-align="center"}
:::
::: {.column}
![[Source](http://www.mackenziemathislab.org/deeplabcut)](https://images.squarespace-cdn.com/content/v1/57f6d51c9f74566f55ecf271/1548190689060-72CRVJXQZ9OK2U7QFM3T/pupil_TV_Scripps.gif){fig-align="center"}
:::
::::

## DeepLabCut: detection and tracking
![](assets/movies/example_tracking_deeplabcut.mp4){width="100%" autoplay="True"}

## DeepLabCut: detection and tracking
![](assets/movies/behavioural-example.mp4){width="100%" autoplay="True"}

## DeepLabCut: cleaning the results
![](assets/images/fish-tracking/eye_x_interpolation1.jpg)

## DeepLabCut: cleaning the results
![](assets/images/fish-tracking/eye_x_interpolation2.jpg)

## Behavioural analysis
### Velocity, distance and activity 1
![](assets/images/fish-tracking/vel_dist1.jpg)

## Behavioural analysis
### Velocity, distance and activity 2
![](assets/images/fish-tracking/vel_dist2.jpg)

## Behavioural analysis
### Peak detection
![](assets/images/fish-tracking/vel_peaks1.jpg)

## Behavioural analysis
### Body facing angle
![](assets/images/fish-tracking/body_angle1.jpg)

## Behavioural analysis
### Body facing angle
![](assets/images/fish-tracking/reference_bodyangle_polar.png){fig-align="center"}

## Fish Socialisation
![](assets/movies/social-example.mp4){width="100%" autoplay="True"}

## Fish Socialisation: Crop + DeepLabCut
![](assets/movies/social-example-deeplabcut.mp4){width="55%" autoplay="True" fig-align="center"}

## Fish Socialisation
### Body facing angle
![](assets/images/fish-tracking/body_angle_histogram_per_genotype.jpg){fig-align="center"}

# What has the future to offer?

## ChatGPT (Open AI) for image analysis
![](https://user-images.githubusercontent.com/1870994/235769990-a281a118-1369-47aa-834a-b491f706bd48.mp4){autoplay="True"}

## Segment anything (Meta AI) {auto-animate="true"}
:::: {.columns}
::: {.column}
![](assets/images/sam1_raw.jpg)
:::
::: {.column}
![](assets/images/sam1.png)
:::
::::

## Segment anything (Meta AI) {auto-animate="true"}
:::: {.columns}
::: {.column}
![](assets/images/sam2_raw.jpg)
:::
::: {.column}
![](assets/images/sam2.png)
:::
::::

## Segment anything for image analysis
![](https://user-images.githubusercontent.com/21022743/230456433-2fa7bc40-a735-4d73-8d87-ecf776bbe2be.mp4){autoplay="True"}


# Thank you {.smaller}

:::: {.columns}
::: {.column  width="60"}
- Find analysis code on out [GitLab group](https://gitlab.com/igbmc/mic-photon)
- Checkout out the [image-analysis-hub blog](https://igbmc.gitlab.io/mic-photon/image-analysis-hub/)
- Recent [Python training material](https://gitlab.com/igbmc/mic-photon/python-intro-igbmc)
- Need help with image analysis? Contact the facility! [groupe-mic-photon@igbmc.fr](mailto:groupe-mic-photon@igbmc.fr)
:::
::: {.column  width="40"}
&#8595; Presentation Slides &#8595;
![](assets/images/qrcode_igbmc.gitlab.io.png)
:::
::::

::: footer
Marco Dalla Vecchia
:::
